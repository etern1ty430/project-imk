<?php 
require_once "config.php";
require_once "conn.php";
require_once "navbar.php";
?>
<html>
    <head>
        <title>Home</title>
        <style>
        
            * {
                margin: 0 auto;
            }
            #box{
                width: 60%;
            }
        </style>
    </head>
    <body>
        <div id="box">
            <h2>History</h2>
            <table id="tabel" border="1" class='table table-striped table-bordered'>
                <thead class="thead-light">
                <tr>
                    <td>Tanggal</td>
                    <td>Alamat</td>
                    <td>Tipe</td>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $query="SELECT * FROM history WHERE username='".$_SESSION['username']."'";
                    $res=mysqli_query($conn, $query);
                    while($row=mysqli_fetch_assoc($res)){
                        ?> 
                        <tr>
                        <td><?php $row['tanggal'] ?></td>
                        <td><?php $row['alamat'] ?></td>
                        <td><?php $row['tipe'] ?></td>
                        </tr>
                        <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </body>
</html>
<script>
$(document).ready( function () {
    $('#tabel').DataTable();
} );
</script>