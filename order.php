<?php 
require_once "config.php";
require_once "conn.php";
require_once "navbar.php";
if(isset($_POST['cancel'])){   
	echo '<script type="text/javascript">
            location.replace("home.php");
          </script>';
}
if(isset($_POST['checkout'])){
    $tanggal=$_POST['tanggal'];
    $alamat=$_POST['alamat'];
    $tipe=$_POST['tipe'];
    $query="INSERT INTO history values('".$_SESSION['username']."','$tanggal','$alamat','$tipe')";
    mysqli_query($conn, $query);
    $_SESSION['total']=$_POST['texttotal'];
	echo '<script type="text/javascript">
            location.replace("takeorder.php");
          </script>';
}
?>
<html>
    <head>
        <title>Order</title>
        <style>
            * {
                margin: 0 auto;
            }
            #box{
                width: 60%;
            }
        </style>
    </head>
    <body>
        <div id="box" >
            <form action="" method="post">
            File: <input type="file" name="file" class="form-control"><br>
            Tipe: <input type="radio" name="tipe" checked value="deliver" >Delivery<input type="radio" name="tipe" value="pickup"  >PickUp<br>
            <div id="deliver">
                Tanggal Kirim: <input type="date" name="tanggal" class="form-control"><br>
                Alamat: <input type="text" name="alamat" class="form-control">
            </div>
            <div id="pickup" style="display:none;">
                Tanggal Ambil: <input type="date" name="tanggal" class="form-control">
            </div><br>
            <div id="detail" style="display:none;">
                Jenis: <select name="jenis" id="jenis" class="form-control">
                    <option>A4</option>
                    <option>A3</option>
                    <option>A2</option>
                </select><br>
                Jumlah Halaman:<input type="number" name="halaman" id="halaman" class="form-control"><br>
                Jumlah Salinan:<input type="number" name="salinan" id="salinan" class="form-control"><br>
                Total: Rp. <label id="total" name="total">0</label>
            </div>
            <div>
                <button name="cancel" class="btn btn-danger">Cancel</button><input type="hidden" id="texttotal" name="texttotal"><input type="hidden" id="textalamat" name="textalamat"><input type="hidden" id="texttanggal" name="texttanggal">
                <button name="checkout" class="btn btn-success">Checkout</button>
                
            </div>
            </form>
        </div>
    </body>
</html>

<script>
    var jenis=$("#jenis").val();
    var hargakertas=250;
    var total=0;
    var kopi=0,halaman=0;
    $("input[type=radio][name=tipe]").change(function(){
        var jenis=this.value;
        if(jenis=="deliver"){
            $("#deliver").css("display","inline");
            $("#pickup").css("display","none");
        }
        else if(jenis=="pickup"){
            $("#deliver").css("display","none");
            $("#pickup").css("display","inline");            
        }
    });
    $("input[type=file]").change(function(){
        $("#detail").css("display","inline");
    });
    $("select[name=jenis]").change(function(){jenis=$("#jenis").val();
                                                         if(jenis=="A4"){hargakertas=250;}
                                                         else if(jenis=="A3"){hargakertas=500;}
                                                         else if(jenis=="A2"){hargakertas=900;}
                                                         hitung();});
    $("input[type=number][name=salinan]").change(function(){kopi=$("#salinan").val();hitung();});
    $("input[type=number][name=halaman]").change(function(){halaman=$("#halaman").val();hitung();});
    $("input[type=date][name=tanggal]").change(function(){$("#texttanggal").val($("#tanggal").val());});
    $("input[type=text][name=alamat]").change(function(){$("#textalamat").val($("#alamat").val());});
    $("input[type=text][name=tipe]").change(function(){$("#texttipe").val($("#tipe").val());});
    function hitung(){
        total=hargakertas*kopi*halaman;
        $("#total").html(total);
        $("#texttotal").attr("value",total);
    }
</script>
