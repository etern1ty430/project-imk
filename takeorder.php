<?php 
require_once "config.php";
require_once "navbar.php";
$total=$_SESSION['total']; 
unset($_SESSION['total']);
?>
<html>
    <head>
        <title>Order</title>
        <style>
            * {
                margin: 0 auto;
            }
        </style>
    </head>
    <body>
        <div id="box" >
            <h3>Terimakasih!</h3>
            <h4>Biaya yang harus dibayarkan: Rp. <?php echo $total ?></h4>
            <p>Jika terdapat ketidakcocokan antara file dengan keterangan yang telah dimasukkan dihalaman sebelumnya, maka order akan dianggap batal</p>
            Cara bayar: <select name="bayar">
                <option>Transfer</option>
                <option>Tunai</option>
            </select>
            <div id="tf">
            <p>Biaya dapat ditransfer pada rekening:<br> 
                Mandiri: 0123 345 123 123 a/n: Print Online Surabaya<br>
                BCA: 0123 345 432 321 a/n: Print Online Surabaya    
            </p>
            </div>
            <div id="tunai">
            <p>Biaya dapat dibayarkan kepada kurir kami yang akan mengirimkan order Anda pada waktu yang telah anda tentukan</p>
            </div>
        </div>
    </body>
</html>

<script>
    $("#tunai").hide();
    $("select[name=bayar]").change(function(){
        var isi =$("select[name=bayar]").val();
        if(isi=="Transfer"){
            $("#tf").show();
            $("#tunai").hide();
        }
        else if(isi=="Tunai"){
            $("#tf").hide();
            $("#tunai").show();
        }
    });
</script>
